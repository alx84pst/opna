Vue.use(VueMask.VueMaskPlugin);

Vue.component('personal-data', {
    props: {
        pcont: Object
    },
    template: '<div><h5>{{ pcont.caption }}</h5><p class="personal-data">{{ pcont.text }}</p></div>',
});

Vue.component('advert-info', {
    props: {
        acont: Object
    },
    template: '<div><h5>{{ acont.caption }}</h5><p class="personal-data">{{ acont.text }}</p></div>',
});

Vue.component('soglasie', {
    props: {
        cont: Object
    },
    data: function() {
        return {
            type: vm.soglasietype,
        };
    },
    template: `
        <div>
            <template v-if="type == 0"><personal-data :pcont="cont.person"></personal-data></template>
            <template v-else-if="type == 1"><advert-info :acont="cont.advert"></advert-info></template>
            <div class="btn__action btn__active btn__right" @click="close()">Закрыть</div>
        </div>
    `,
    methods: {
        close: function() {
            vm.soglasie.modal = !vm.soglasie.modal;
        }
    }
});

Vue.component('soglasie-checkboxes', {
    props: {
        sg: Object,
    },
    template: `
        <div class="checkbox">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">    
                    <label>
                        <input type="checkbox" @click.self="$emit('changePer')" :checked="sg.personal"><span class="checkbox__icon"></span> Согласие на
                    </label><span @click="setType(0)">&nbsp;<u>обработку персональных данных</u></span>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <label>
                        <input type="checkbox" @click.self="$emit('changeAdv')" :checked="sg.advert"><span class="checkbox__icon"></span> Согласие на
                    </label><span @click="setType(1)">&nbsp;<u>получение рекламной информации от Audi</u></span>
                </div>
            </div>
        </div>
    `,
    methods: {
        // type (number) -- 0 - personal-data, 1 - advert-info
        setType: function(type) {
            vm.soglasietype = type;
            vm.soglasie.modal = true;
        },
        checked: function() {

        }
    }
});

Vue.component('model-list-cell', {
    props: {
        list: Object
    },
    template: `
        <li @click="setModel(list.id)">
            <template v-if="list.active">
                <span class="active">
                    <template v-if="list.rs"><span class="rs"><a href="#">{{ list.name }}</a></span> </template>
                    <template v-else><a href="#">{{ list.name }}</a></template>
                </span>
            </template><template v-else>
                <span>
                    <template v-if="list.rs"><span class="rs"><a href="#">{{ list.name }}</a></span></template>
                    <template v-else><a href="#">{{ list.name }}</a></template>
                </span>
            </template>
        </li>
    `,
    methods: {
        setModel: function(id) {
            var active = vm.models[id].active;
            if (active != true) {
                vm.models[id].active = true;
                vm.selectmodel = -1;
                vm.model.length = 0;
            } else {
                if (vm.selectmodel != -1) {
                    vm.models[vm.selectmodel].active = true;
                }
                vm.models[id].active = false;
                vm.selectmodel = id;
                vm.model.length = 0;
                vm.model.push(vm.models[id].name);
            }
        }
    }
});

Vue.component('record-on-test-drive', {
    props: {
        title: String
    },
    template: '<p @click="showModal(2)"><a href="#">{{ title }}</a></p>',
    methods: {
        showModal: function(id) {
            vm.showModal(id);
        }
    }
});

Vue.component('car-list-cell', {
    props: {
        list: Object,
        title: String
    },
    data: function() {
        return {
            prohibited: App.price.prohibited,
            replase: App.price.arc_replace
        };
    },
    template: `
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="car-sale-item">
                <div class="car-image" :style="list.image" @click="showModal(0, list.id)"></div>
                <div class="car-name">
                    <div class="car-name__model" @click="showModal(0, list.id)"><span class="left-space">{{ list.name }}</span></div>
                    <div class="car-name__number">Комм.номер: {{ list.commercial_number }}, {{ list.status }}</div>
                </div>
                <div class="car-price">
                    <div><span class="left-space">{{ list.official_price }} рублей</span></div>
                    <div v-if="list.special_price != '0' && replase == false">
                        <span class="car-price__special left-space">{{ list.special_price }} рублей</span>
                    </div>
                    <div v-else>
                        <span class="car-price__special car-price__prohibited left-space">{{ prohibited }}</span>
                    </div>
                </div>
                <div class="car-action">
                    <div class="btn__action btn__active" @click="showModal(1, {id:list.id, commercial_number:list.commercial_number, name:list.name})">{{ title }}</div>
                </div>
            </div>
        </div>
    `,
    methods: {
        showModal: function(id, carid = 0) {
            vm.showModal(id);
            if (id == 0) {
                vm.getCarComplectation(carid);
            } else if (id == 1) {
                this.setCarBuy(carid);
            }
        },
        setCarBuy: function(arr) {
            vm.cartobuy.push(arr);
        }
    }
});

Vue.component('replace-models', {
    props: {
        text: String,
    },
    template: `
        <div class="row">
            <div class="col-lg-12"><div class="space__1"></div></div>
            <div class="col-lg-12"><p class="text-info">{{ text }}</p></div>
            <div class="col-lg-12"><div class="space__1"></div></div>
        </div>
    `,
});

Vue.component('show-more-cars', {
    props: {
        comquantity: Number,
        curquantity: Number,
        increment: Number,
    },
    data: function() {
        return {
            showmore: "Показать еще",
            showfrom: "из",
        };
    },
    computed: {
        showincrement: function() {
            var self = this;
            if ((self.comquantity - self.curquantity) < self.increment) {
                return self.comquantity - self.curquantity;
            } else {
                return self.increment;
            }
        },
        showquantity: function() {
            var self = this;
            return self.comquantity - self.curquantity;
        },
    },
    template: `
        <div class="col-lg-12 text-center">
            <span class="showmore" @click="increaseShowQuantity(showincrement)">{{ showmore }} {{ showincrement }} {{ showfrom }} {{ showquantity }}</span>
        </div>
    `,
    methods: {
        increaseShowQuantity: function(num) {
            vm.showquantity = vm.showquantity + num;
        }
    }
});

Vue.component('complectation', {
    computed: {
        content: function() {
            return vm.complectation;
        },
        loader: function() {
            return vm.complectationloader;
        }
    },
    template: `
        <div class="container-fluid complectation"><div class="row">
            <template v-if="loader">
                <div class="col-lg-12"><h3>Комплектация</h3></div>
                <div class="col-lg-12 complectation-loader-image"><div class="text-center"><div class="loader"></div></div></div>
                <div class="col-lg-12 complectation-loader-text"><div class="text-center">Получение комплектации автомомбиля</div></div>
            </template>
            <template v-else>
                <div class="col-lg-12 complectation-header">
                    <h3>Комплектация {{ content.name }}<br><small>Комм.номер: {{ content.commercial_number }}</small></h3>
                </div>
                <div
                    is="complectation-list"
                    v-for="item in content.complectation_summary"
                    :list="item"
                    :key="item.column"
                ></div>
            </template>
        </div></div>
    `,
});

Vue.component('complectation-list', {
    props: {
        list: Object,
    },
    template: `
        <div class="col-lg-12"><div class="row">
            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 complectation-list-caption">{{ list.column }}</div>
            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 complectation-list-summary">{{ list.value }}</div>
        </div></div>
    `,
});

Vue.component('common-fields', {
    props: {
        cf: Object,
        ce: Object,
    },
    data: function() {
        return {
            tel: "+7 (###) ###-##-##",
            name: this.cf.name,
            surname: this.cf.surname,
            phone: this.cf.phone,
            email: this.cf.email,
        };
    },
    template: `
        <div>
            <div class="form-group">
                <label for="inputName" class="col-sm-4 control-label">Ваше имя:</label>
                <div class="col-sm-8">
                    <input type="text" v-model="name" class="form-control" id="inputName">
                    <div class="filderror" v-show="ce.name.show"><span class="text-danger">{{ ce.name.text }}</span></div>
                </div>
            </div><div class="form-group">
                <label for="inputSurename" class="col-sm-4 control-label">Ваша фамилия:</label>
                <div class="col-sm-8">
                    <input type="text" v-model="surname" class="form-control" id="inputSurename">
                    <div class="filderror" v-show="ce.surname.show"><span class="text-danger">{{ ce.surname.text }}</span></div>
                </div>
            </div><div class="form-group">
                <label for="inputTel" class="col-sm-4 control-label">Телефон:</label>
                <div class="col-sm-8">
                    <input type="text" v-mask="tel" v-model="phone" class="form-control" id="inputTel">
                    <div class="filderror" v-show="ce.phone.show"><span class="text-danger">{{ ce.phone.text }}</span></div>
                </div>
            </div><div class="form-group">
                <label for="inputEmail" class="col-sm-4 control-label">Email:</label>
                <div class="col-sm-8">
                    <input type="email" v-model="email" class="form-control" id="inputEmail">
                    <div class="filderror" v-show="ce.email.show"><span class="text-danger">{{ ce.email.text }}</span></div>
                </div>
            </div>
        </div>
    `,
    watch: {
        name: function() {
            this.checkForm("name");
        },
        surname: function() {
            this.checkForm("surname");
        },
        phone: function() {
            this.checkForm("phone");
        },
        email: function() {
            this.checkForm("email");
        },
    },
    methods: {
        checkForm: function(field) {
            switch (field) {
                case "name":
                    if (this.name == "") {
                        vm.modalcommerrors.name.text = "Вы не ввели ваше имя";
                        // vm.modalcommerrors.name.show = (field == "name") ? true : false;
                    } else {
                        vm.modalcommfilds.name = this.name;
                        vm.modalcommerrors.name.text = "";
                        // vm.modalcommerrors.name.show = false;
                    }
                    break;

                case "surname":
                    if (this.surname == "") {
                        vm.modalcommerrors.surname.text = "Вы не ввели вашу фамилию";
                        // vm.modalcommerrors.surname.show = (field == "surname") ? true : false;
                    } else {
                        vm.modalcommfilds.surname = this.surname;
                        vm.modalcommerrors.surname.text = "";
                        // vm.modalcommerrors.surname.show = false;
                    }
                    break;

                case "phone":
                    if (this.phone == "") {
                        vm.modalcommerrors.phone.text = "Вы не ввели телефонный номер";
                        // vm.modalcommerrors.phone.show = (field == "phone") ? true : false;
                    } else {
                        if (!vm.validPhone(this.phone)) {
                            vm.modalcommfilds.phone = this.phone;
                            vm.modalcommerrors.phone.text = "Вы ввели некорректный номер телефона";
                            // vm.modalcommerrors.phone.show = (field == "phone") ? true : false;
                        } else {
                            vm.modalcommfilds.phone = this.phone;
                            vm.modalcommerrors.phone.text = "";
                            // vm.modalcommerrors.phone.show = false;
                        }
                    }
                    break;

                case "email":
                    if (this.email == "") {
                        vm.modalcommerrors.email.text = "Вы не ввели указали адрес электронной почты";
                        // vm.modalcommerrors.email.show = (field == "email") ? true : false;
                    } else {
                        if (!vm.validEmail(this.email)) {
                            vm.modalcommfilds.email = this.email;
                            vm.modalcommerrors.email.text = "Вы ввели некорректный адрес электронной почты";
                            // vm.modalcommerrors.email.show = (field == "email") ? true : false;
                        } else {
                            vm.modalcommfilds.email = this.email;
                            vm.modalcommerrors.email.text = "";
                            // vm.modalcommerrors.email.show = false;
                        }
                    }
                    break;
            }
        },
    }
});

Vue.component('btn-send', {
    props: {
        active: Boolean,
        form: String,
        cf: Object,
        ce: Object,
    },
    data: function() {
        return {
            add: {},
            action: false,
            request: false,
        };
    },
    template: `
        <div>
            <template v-if="!request">
                <div class="btn__action btn__right" :class="{btn__active: active}" @click="sendOrder(active, form)">Отправить заявку</div>
            </template>
            <template v-else>
                <div class="btn__action btn__right">Обработка запроса...</div>
            </template>
        </div>
    `,
    methods: {
        sendOrder: function(active, form) {
            if (active) {
                switch (form) {
                    case "test":
                        this.checkFilds();
                        this.addCheckForm("selectCar");
                        this.changeAction();
                        if (this.action) {
                            console.log("follow");
                            this.request = !this.request;
                            this.testDriveRequest();
                        } else {
                            console.log("abort");
                        }
                        break;

                    case "pred":
                        this.checkFilds();
                        this.changeAction();
                        if (this.action) {
                            console.log("follow");
                            this.request = !this.request;
                            this.callBackRequest();
                        } else {
                            console.log("abort");
                        }
                        break;
                }
            }
        },
        changeAction: function() {
            for (var key1 in this.cf) {
                if (this.cf.hasOwnProperty(key1)) {
                    this.action = (this.cf[key1] != "") ? true : false;
                    if (!this.action) break;
                }
            }
            if (this.action) {
                for (var key2 in this.ce) {
                    if (this.ce.hasOwnProperty(key2)) {
                        this.action = (this.ce[key2].text == "") ? true : false;
                        if (!this.action) break;
                    }
                }
            }
        },
        checkFilds: function() {
            var self = this;
            if (document.getElementById("inputName").value == "") {
                vm.modalcommerrors.name.text = "Вы не ввели ваше имя";
            } else {
                vm.modalcommerrors.name.text = "";
                vm.modalcommfilds.name = document.getElementById("inputName").value;
            }
            if (document.getElementById("inputSurename").value == "") {
                vm.modalcommerrors.surname.text = "Вы не ввели вашу фамилию";
            } else {
                vm.modalcommerrors.surname.text = "";
                vm.modalcommfilds.surname = document.getElementById("inputSurename").value;
            }
            if (document.getElementById("inputTel").value == "") {
                vm.modalcommerrors.phone.text = "Вы не ввели телефонный номер";
            } else {
                if (!vm.validPhone(document.getElementById("inputTel").value)) {
                    vm.modalcommerrors.phone.text = "Вы ввели некорректный номер телефона";
                } else {
                    vm.modalcommerrors.phone.text = "";
                    vm.modalcommfilds.phone = document.getElementById("inputTel").value;
                }
            }
            if (document.getElementById("inputEmail").value == "") {
                vm.modalcommerrors.email.text = "Вы не ввели указали адрес электронной почты";
            } else {
                if (!vm.validEmail(document.getElementById("inputEmail").value)) {
                    vm.modalcommerrors.email.text = "Вы ввели некорректный адрес электронной почты";
                } else {
                    vm.modalcommerrors.email.text = "";
                    vm.modalcommfilds.email = document.getElementById("inputEmail").value;
                }
            }
        },
        addCheckForm: function(filds) {
            if (typeof(filds) == 'string') {
                this.add[filds] = document.getElementById(filds).value;
            } else if (typeof(fields) == 'object') {
                console.log("disabled");
            }
        },
        testDriveRequest: function() {
            var params = new URLSearchParams();
            params.append('model-page', vm.advertmodel);
            params.append('request', 'testdrive');
            params.append('landing', '1');
            params.append('id', this.add.selectCar);
            params.append('comment', '');
            params.append('center', '1');
            params.append('soglasieper', vm.soglasie.personal);
            params.append('soglasieadv', vm.soglasie.advert);
            params.append('name', vm.modalcommfilds.name);
            params.append('surname', vm.modalcommfilds.surname);
            params.append('phone', vm.modalcommfilds.phone);
            params.append('phone_type', '2');
            params.append('email', vm.modalcommfilds.email);
            axios.post('./requests.php', params)
                .then(function(response) {
                    console.log(response);
                    this.request = !this.request;
                    vm.modalshow = false;
                    vm.modalcontent = 4;
                    vm.modalshow = true;
                })
                .catch(function(error) {
                    this.request = !this.request;
                    console.log(error);
                });
        },
        callBackRequest: function() {
            var params = new URLSearchParams();
            params.append('model-page', vm.advertmodel);
            params.append('request', 'callback');
            params.append('id', vm.cartobuy[0].id);
            params.append('commercial_number', vm.cartobuy[0].commercial_number);
            params.append('comment', '');
            params.append('center', '1');
            params.append('soglasieper', vm.soglasie.personal);
            params.append('soglasieadv', vm.soglasie.advert);
            params.append('name', vm.modalcommfilds.name);
            params.append('surname', vm.modalcommfilds.surname);
            params.append('phone', vm.modalcommfilds.phone);
            params.append('phone_type', '2');
            params.append('email', vm.modalcommfilds.email);
            params.append('type', 'new-cars');
            axios.post('./requests.php', params)
                .then(function(response) {
                    console.log(response);
                    this.request = !this.request;
                    vm.modalshow = false;
                    vm.modalcontent = 4;
                    vm.modalshow = true;
                })
                .catch(function(error) {
                    this.request = !this.request;
                    console.log(error);
                });
        }
    }
});

Vue.component('spetspredlozhenie', {
    props: {
        sg: Object,
        cf: Object,
        ce: Object,
    },
    data: function() {
        return {
            per: false,
            adv: false,
            active: false,
            form: "pred",
            car: vm.cartobuy[0].name,
        };
    },
    template: `
        <div class="container-fluid"><div class="row">
            <template v-if="sg.modal"><soglasie :cont="sg.content"></soglasie></template>
            <template v-else>
                <div class="col-lg-12"><h3>Получить спецпредложение</h3></div>
                <div class="col-lg-12">
                    <form class="form-horizontal">
                        <common-fields :cf="cf" :ce="ce"></common-fields>
                        <div class="form-group">
                            <label for="inputCar" class="col-sm-4 control-label">Автомобиль:</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" v-model="car" id="inputCar" disabled>
                                <div class="filderror" v-show="ce.name.show"><span class="text-danger">{{ ce.name.text }}</span></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
                                <soglasie-checkboxes :sg="sg" @changePer="per=!per" @changeAdv="adv=!adv"></soglasie-checkboxes>
                            </div><div class="col-lg-5 col-md-12 col-sm-12 col-xs-12">
                                <btn-send :active="active" :form="form" :cf="cf" :ce="ce"></btn-send>
                            </div>
                        </div>
                    </form>
                </div>
            </template>
        </div></div>
    `,
    watch: {
        per: function() {
            this.changeActive();
            this.sg.personal = !this.sg.personal;
        },
        adv: function() {
            this.changeActive();
            this.sg.advert = !this.sg.advert;
        }
    },
    methods: {
        changeActive: function() {
            var self = this;
            self.active = (self.adv && self.per) ? true : false;
        }
    },
});

Vue.component('test-drive', {
    props: {
        sg: Object,
        cf: Object,
        ce: Object,
    },
    data: function() {
        return {
            cars: vm.testcars,
            per: false,
            adv: false,
            active: false,
            form: "test",
        };
    },
    template: `
        <div class="container-fluid"><div class="row">
            <template v-if="sg.modal"><soglasie :cont="sg.content"></soglasie></template>
            <template v-else>
                <div class="col-lg-12"><h3>Запись на тест-драйв</h3></div>
                <div class="col-lg-12">
                    <form class="form-horizontal">
                        <common-fields :cf="cf" :ce="ce"></common-fields>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-4 control-label">Автомобиль:</label>
                            <div class="col-sm-8">
                                <select class="form-control" id="selectCar">
                                    <option
                                        is="test-drive-car-list"
                                        v-for="item in cars"
                                        :list="item"
                                        :key="item.id"
                                    ></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-7 col-lg-7 col-lg-7 col-lg-7">
                                <soglasie-checkboxes :sg="sg" @changePer="per=!per" @changeAdv="adv=!adv"></soglasie-checkboxes>
                            </div><div class="col-lg-5">
                                <btn-send :active="active" :form="form" :cf="cf" :ce="ce"></btn-send>
                            </div>
                        </div>
                    </form>
                </div>
            </template>
        </div></div>
    `,
    watch: {
        per: function() {
            this.changeActive();
            this.sg.personal = !this.sg.personal;
        },
        adv: function() {
            this.changeActive();
            this.sg.advert = !this.sg.advert;
        }
    },
    methods: {
        changeActive: function() {
            var self = this;
            self.active = (self.adv && self.per) ? true : false;
        }
    },
});

Vue.component('test-drive-car-list', {
    props: {
        list: Object,
    },
    template: '<option :value="list.id">{{ list.name }}</option>',
});

Vue.component('centre-contacts', {
    data: function() {
        return {
            c: vm.contacts
        };
    },
    template: `
        <div class="container-fluid"><div class="row"><div class="col-lg-12">
            <h3>{{ c.name }}<br><small>{{ c.desc }}</small></h3>
            <p class="contacts-text">{{ c.addr }}<br>Телефон: {{ c.telf }}</p>
            <p class="contacts-working-time"><span class="text-muted">График работы:<br>{{ c.time.service }} сервис и запчасти<br>{{ c.time.sale }} продажи автомобилей</span></p>
        </div></div></div>
    `,
});

Vue.component('answer-after-request', {
    template: `
        <div class="container-fluid"><div class="row"><div class="col-lg-12">
            <h3>Ваш запрос обработан</h3>
            <p class="text-success">Спасибо за обращение. Специалист отдела продаж свяжется с Вами в ближайшее время</p>
            <div class="btn__action btn__active btn__right" @click="close()">Закрыть</div>
        </div></div></div>
    `,
    methods: {
        close: function() {
            vm.modalshow = !vm.modalshow;
        }
    }
});

Vue.component('page-modal', {
    props: {
        ms: Boolean,
        mc: Number,
        sg: Object,
        cf: Object,
        ce: Object,
    },
    template: `
        <div class="modal-wrapper" v-if="ms">
            <div class="modal">
                <template v-if="!sg.modal">   
                    <div class="modal-header" @click="$emit(\'close\')">
                        <span class="modal-close">×</span>
                    </div>
                </template>
                <div class="modal-content">
                    <template v-if="mc == 0">
                        <complectation></complectation>
                    </template>
                    <template v-else-if="mc == 1">
                        <spetspredlozhenie :sg="sg" :cf="cf" :ce="ce"></spetspredlozhenie>
                    </template>
                    <template v-else-if="mc == 2">
                        <test-drive :sg="sg" :cf="cf" :ce="ce"></test-drive>
                    </template>
                    <template v-else-if="mc == 3">
                        <centre-contacts></centre-contacts>
                    </template>
                    <template v-else-if="mc == 4">
                        <answer-after-request></answer-after-request>
                    </template>
                </div>
            </div>
        </div>
    `,
});

Vue.component('yandex-map', {
    template: '<div id="map"></div>',
});

var vm = new Vue({
    el: '#app',
    data: {
        model: [], // ключ модели желаемого а/м
        cars: [], // полный список а/м для продажи, включая а/м без спеццены
        testcars: [], // список а/м для тест-драйва
        cartobuy: [], // данные а/м для спецпредложения
        currency: 'рублей',
        advertmodel:App.modal.model,
        models: [ // список моделей а/м для фильтра
            { id: 0, name: "A3", active: true, rs: false, replace: ["A4", "Q3"] },
            { id: 1, name: "A4", active: true, rs: false, replace: ["A5", "A6", "Q5"] },
            { id: 2, name: "A5", active: true, rs: false, replace: ["A4", "A6", "A7", "Q5"] },
            { id: 3, name: "A6", active: true, rs: false, replace: ["A5", "A7"] },
            { id: 4, name: "A7", active: true, rs: false, replace: ["A6", "A8"] },
            { id: 5, name: "A8", active: true, rs: false, replace: ["Q7", "Q8"] },
            { id: 6, name: "Q3", active: true, rs: false, replace: ["A4", "Q5"] },
            { id: 7, name: "Q5", active: true, rs: false, replace: ["Q7"] },
            { id: 8, name: "Q7", active: true, rs: false, replace: ["Q8"] },
            { id: 9, name: "RS", active: true, rs: true, replace: [] }
        ],
        replacephrase: "На данный момент автомобилей данной модели нет в наличии, но мы рады предложить Вам альтернативу на безупречных условиях.",
        replace: false, // статус
        complectation: [],
        complectationloader: true, // статус
        selectmodel: -1, // выбранная модель а/м
        showquantity: 9, // количество а/м для показа
        showincrement: 9, // количество а/м для увеличения количества показа
        loader: true, // статус показа основного загрузчика
        modalshow: false, // статус показа модального окна
        modalcontent: 0, // 0 - Комплектация, 1 - Спецпредложение, 2 - Тест-драйв, 3 - Контактные данные Центра, 4 - Ответ клиенту
        modalcommfilds: { // общие поля форм заявок
            name: "",
            surname: "",
            phone: "",
            email: "",
        },
        modalcommerrors: { // ошибки и статусы их показа
            name: { text: "", show: true },
            surname: { text: "", show: true },
            phone: { text: "", show: true },
            email: { text: "", show: true },
        },
        soglasie: { // статусы согласий
            modal: false,
            personal: false,
            advert: false,
            content: App.modal.soglasie
        },
        soglasietype: 0, // 0 - personal-data, 1 - advert-info
        contacts: App.modal.contacts,
        config: [],
    },
    computed: {
        filteredCars: function() {
            var self = this;
            var arr = self.cars;
            var mod = self.model;
            if (mod.length == 0) {
                return arr.filter(function(car, i, arr) {
                    return car.official_price != '0';
                });
            } else {
                return arr.filter(function(car, i, arr) {
                    var foo = function(elm) {
                        if (car.model != mod[0]) {
                            return elm == car.parent;
                        } else {
                            return elm == car.model;
                        }
                    };
                    return mod.some(foo) && car.official_price != '0';
                });
            }
        },
        limitedCars: function() {
            var self = this;
            var arr = self.filteredCars;
            if (self.showquantity > 0) {
                return arr.filter(function(car, i, arr) {
                    return i < self.showquantity;
                });
            }
        },
    },
    watch: {
        modalshow: function(val, oldVal) {
            vm.overlayFadeIn();
            if (!val) {
                vm.soglasie.personal = false;
                vm.soglasie.advert = false;
                vm.cartobuy.length = 0;
            }
        },
        selectmodel: function(val, oldVal) {
            if (val != -1 && vm.filteredCars.length == 0) {
                vm.model = vm.models[val].replace;
                vm.replace = true;
            } else {
                vm.replace = false;
            }
        },
    },
    mounted: function() {
        this.$nextTick(function() {
            vm.getCarModels();
            vm.getCarList();
            vm.getTestCarList();
        });
    },
    methods: {
        setModel: function(name) {
            console.log(name);
            var id = null;
            for (var i=0; i < vm.models.length; i++) {
                id = (vm.models[i].name == name)? i: null;
                if (id != null) {
                    break;
                }
            }
            console.log(id);
            if (id != null) {
                if (!vm.models[id].active) {
                    vm.models[id].active = true;
                    vm.selectmodel = -1;
                    vm.model.length = 0;
                } else {
                    if (vm.selectmodel != -1) {
                        vm.models[vm.selectmodel].active = true;
                    }
                    vm.models[id].active = false;
                    vm.selectmodel = id;
                    vm.model.length = 0;
                    vm.model.push(vm.models[id].name);
                }
            }
        },
        getCarList: function() {
            axios.post('cars.php')
                .then(function(response) {
                    vm.cars = response.data.ITEMS;
                    vm.loader = false;
                    vm.showYandexMap();
                    console.log(vm.advertmodel);
                    vm.setModel(vm.advertmodel);
                })
                .catch(function(error) {
                    console.log(error);
                });
        },
        getTestCarList: function() {
            axios.post('testcars.php')
                .then(function(response) {
                    vm.testcars = response.data.ITEMS;
                })
                .catch(function(error) {
                    console.log(error);
                });
        },
        getCarComplectation: function(id) {
            vm.complectationloader = true;
            var params = new URLSearchParams();
            params.append('CARID', id);
            axios.post('complectation.php', params)
                .then(function(response) {
                    vm.complectation = response.data;
                    vm.complectationloader = false;
                })
                .catch(function(error) {
                    console.log(error);
                    vm.complectationloader = false;
                });
        },
        getCarModels: function() {
            axios.post('carmodelsget.php')
                .then(function(response) {
                    vm.models = response.data.ITEMS;
                })
                .catch(function(error) {
                    console.log(error);
                });
        },
        overlayFadeIn: function() {
            var overlay = document.getElementsByClassName("overlay");
            if (overlay.length != 0) {
                document.body.removeChild(overlay[0]);
            } else {
                overlay = document.createElement("div");
                overlay.className = "overlay fade in";
                document.body.appendChild(overlay);
            }
        },
        showModal: function(id) {
            vm.modalshow = !vm.modalshow;
            vm.modalcontent = id;
        },
        validEmail: function(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        },
        validPhone: function(phone) {
            var reg = /\d/g;
            var result = [];
            result = phone.match(reg);
            return (result.length == 11) ? true : false;
        },
        showYandexMap: function() {
            ymaps.ready(function() {
                var myMap = new ymaps.Map('map', {
                    center: [59.93607602, 30.35089660],
                    zoom: 11,
                    controls: ['zoomControl', 'fullscreenControl']
                }, { suppressMapOpenBlock: true });

                myMap.behaviors.disable('scrollZoom');

                var placemarkImage = {
                    inactive: '/bitrix/templates/cabinet.phoenix-motors/assets/png/pointer.png',
                    active: '/bitrix/templates/cabinet.phoenix-motors/assets/png/pointer-active.png'
                };

                var placemarkOptions = {
                    iconLayout: 'default#image',
                    iconImageHref: placemarkImage.inactive,
                    iconImageSize: [16, 26],
                    iconImageOffset: [-8, -26],
                    balloonLayout: 'default#imageWithContent',
                    balloonImageOffset: [-8, -56],
                    hideIconOnBalloonOpen: false,
                    openBalloonOnClick: false,
                    cursor: 'pointer'
                };

                var props = {
                    name: 'acp',
                    content: '<b>Ауди Центр Петербург</b><br>Стачек пр., 106',
                    coors: [59.86091576, 30.26236450]
                };

                var options = placemarkOptions;

                var point = new ymaps.Placemark(props.coors, { balloonContent: props.content }, options);

                props.placemark = point;
                myMap.geoObjects.add(point);

                point.events.add('click', function(event) {
                    vm.showModal(3);
                });
            });
        }
    }
});